import pickle 
import argparse
import os, sys, pickle 
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from utils import scoring_func 
from collections import OrderedDict
import nltk 
nltk.download('stopwords')
from nltk.corpus import stopwords 
from rank_bm25 import BM25Okapi 
import multiprocessing
from itertools import repeat 


parser = argparse.ArgumentParser()
parser.add_argument("--system")
parser.add_argument("--user_data")
parser.add_argument("--user_data_type")
parser.add_argument("--forum_data_type")
parser.add_argument("--save_path")
args = parser.parse_args()

# compute score using bm25  
def bm25_compute_relevance(f_case, f_idx, bm25, url_list, stackoverflow_corpus, args):
    
    result_dict, result_sent_dict = {}, {}
    
    input = args.user_data
    input_tokens = input.split(' ')
    scores = bm25.get_scores(input_tokens)
    for idx, score in enumerate(scores):
        url = url_list[idx] 
        if url in result_dict:
            if result_dict[url] < score:
                result_dict[url] = score 
        else:
            result_dict[url] = score 

    # higher score the better result 
    result_dict = OrderedDict(sorted(result_dict.items(), key = lambda item:item[1], reverse = True))
    with open(f"{args.save_path}/tmp_{f_idx}.pickle", "wb") as fw:
        pickle.dump(result_dict, fw)
    
    idx = 0
    ranking_dict = scoring_func.get_ranked_dict(result_dict)
    with open(f'./{args.save_path}/{args.user_data_type}_{args.forum_data_type}.pickle', "wb") as fw:
        pickle.dump(ranking_dict, fw) 

def bm25_dataset(data):
    
    bm25_corpus, url_list = [], []
    for d in data:
        url = d['url']
        text = d['text']
        tokens = text.split(' ')
        bm25_corpus.append(tokens)
        url_list.append(url)
        
    bm25 = BM25Okapi(bm25_corpus)
    return bm25, url_list, bm25_corpus 

def bm25_main(args):
        
    #preprocessed stackoverflow post 
    data_path = f"../data/{args.system}/{args.forum_data_type}.pickle"
    with open(data_path, "rb") as fr:
        post = pickle.load(fr)
    
    post_bm25, url_list, post_corpus = bm25_dataset(post)
    
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path, exist_ok=True)
        
    bm25_compute_relevance(post_bm25, url_list, post_corpus, args)
    
bm25_main(args)
