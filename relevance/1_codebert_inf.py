import pickle 
import argparse
import os
from itertools import repeat 
import tensorflow as tf 
import torch 
from transformers import RobertaForSequenceClassification, RobertaTokenizer
import sys 
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from utils import scoring_func 
from collections import OrderedDict
from nltk.tokenize import sent_tokenize 

parser = argparse.ArgumentParser()
parser.add_argument("--system")
parser.add_argument("--user_data_type")
parser.add_argument("--user_data")
parser.add_argument("--forum_data_type")
parser.add_argument("--save_path")
parser.add_argument("--device_id", type=int)
parser.add_argument("--model_name_or_path")
parser.add_argument("--batch_size")
parser.add_argument("--tokenizer_name")
args = parser.parse_args()

def compute_relevance_between_user_and_stackoverflow(batch_size, url_list, input_list, post, model, tokenizer, turn):

    prob_list = []
    result_dict = {}
    with torch.no_grad():
        softmax = tf.keras.layers.Softmax()
        start_idx = 0 
        for t in range(0, turn + 1):
        
            end_idx = start_idx + batch_size 
            if t == turn:
                end_idx = len(post)
            
            if args.user_data_type == "cod":
                batch_c_list = post[start_idx:end_idx]
                batch_u_list = input_list[start_idx:end_idx]
            else:
                batch_c_list = input_list[start_idx:end_idx] 
                batch_u_list = post[start_idx:end_idx]
                    
            start_idx = end_idx 
            
            if len(batch_u_list) == 0:
                break 
            # tokenize input           
            tokenizer_output = tokenizer(batch_c_list, batch_u_list, truncation = True, padding =True, max_length = 512, return_tensors = "pt")
            tokenizer_output.to(args.device)
            # get logit from model 
            logits = model(**tokenizer_output).logits 
            logits = logits.detach().cpu().numpy()
            # logit to probability 
            prob = softmax(logits)
            
            if t == turn:
                batch_size = len(prob)
            for i in range(batch_size):
                p = prob[i]
                p = p.numpy()
                prob_list.append(p)
            
        for p_i, prob in enumerate(prob_list):
            prob = prob[1]
            p_url = url_list[p_i]
   
            if p_url in result_dict:
                if result_dict[p_url] < prob:
                    result_dict[p_url] = prob 
            else:
                result_dict[p_url] = prob   
    return result_dict 

def codebert_compute_relevance(post, url_list, args):
        
    batch_size = int(args.batch_size) 

    tokenizer = RobertaTokenizer.from_pretrained(args.tokenizer_name)
    model = RobertaForSequenceClassification.from_pretrained(args.model_name_or_path)
    model.to(args.device)
    model.eval()
        
    # tokenize user data into sentences
    if args.user_data_type != "cod": 
        user_data = sent_tokenize(args.user_data)
    else:
        user_data = [args.user_data]
    
    total_result_dict = {}
    for sent in user_data:
        
        turn = int(len(post) / batch_size)
        input_list = [sent for i in range(len(post))]
        
        result_dict = compute_relevance_between_user_and_stackoverflow(batch_size, url_list, input_list, post, model, tokenizer, turn)
        for url, value in result_dict.items():
            if url in total_result_dict:
                total_result_dict[url] = total_result_dict[url] + value 
            else:
                total_result_dict[url] = value 
    total_result_dict = OrderedDict(sorted(total_result_dict.items(), key = lambda item:item[1], reverse = True))
            
    ranking_dict = scoring_func.get_ranked_dict(total_result_dict)
    
    # return ranking_dict 
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path)
        
    with open('../{args.save_path}/{args.user_data_type}_{args.forum_data_type}.pickle', "wb") as fw:
        pickle.dump(ranking_dict, fw)

def codebert_main(args):
    
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = f"{args.device_id}"
    device = "cuda:0"
    args.device = device 
    
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path, exist_ok=True)
    
    #preprocessed stackoverflow post 
    data_path = f"../data/{args.system}/{args.forum_data_type}.pickle"
    with open(data_path, "rb") as fr:
        post = pickle.load(fr)
        
    url_list, post_list = [], []
    for p in post:
        url_list.append(p['url'])
        post_list.append(p['text'])

    #sbert_compute_relevance(benchmark_cases[0], post_list, url_list, args)   
    codebert_compute_relevance(post_list, url_list, args)

codebert_main(args)
