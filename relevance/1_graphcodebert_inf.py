import pickle 
from transformers import RobertaForSequenceClassification, RobertaTokenizer, RobertaModel
import tensorflow as tf 
from collections import OrderedDict
from itertools import repeat 
import os, sys
import torch 
import multiprocessing
import argparse 
import logging 
import sys 
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from utils import scoring_func 
from model import Model, TextDataset, convert_examples_to_features, InputFeatures, extract_dataflow
from torch.utils.data import DataLoader, Dataset, SequentialSampler, RandomSampler, TensorDataset
import numpy as np 
from nltk.tokenize import sent_tokenize 

logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument("--system")
parser.add_argument("--model_path")
parser.add_argument("--user_data")
parser.add_argument("--user_data_type")
parser.add_argument("--forum_data_type")
parser.add_argument("--batch_size", type=int)
parser.add_argument("--user_data_length", type=int)
parser.add_argument("--forum_data_length", type=int)
parser.add_argument("--device_id", type=int)
parser.add_argument("--model_type")
parser.add_argument("--lang")
parser.add_argument("--save_path")
args = parser.parse_args()
    
cpu_cont = 16
pool = multiprocessing.Pool(cpu_cont)
tensor_limit = 10000
qaset_limit = 10000

def check_dataflow_parser(QAset, args):
    
    n_QAset_result = []
    tokenizer = RobertaTokenizer.from_pretrained('roberta-base')
    for q_idx, question in enumerate(QAset):
        item = (q_idx, question, "cod", args, tokenizer)
        parse_result = convert_examples_to_features(item)
        # can't parse the dataflow of code 
        if parse_result != "None":
            n_QAset_result.append(question) 
    
    return n_QAset_result

def matrix_multiplication(a_vecs, b_vecs, result_dict, t, url_list):

    scores = np.matmul(a_vecs, b_vecs.T)
    scores = scores[0]
    for idx, score in enumerate(scores):
        q_url = url_list[t * args.tensor_limit + idx]
        if q_url == args.base_url:
            print(score)
        if q_url in result_dict:
            if result_dict[q_url] < score:
                result_dict[q_url] = score 
                if q_url == args.base_url:
                    print(score)
        else:
            result_dict[q_url] = score 
    return result_dict 

def classify_code_and_questions(user_data, args, QAset, result_dict):
    
    #load tokenizer and trained model 
    tokenizer = RobertaTokenizer.from_pretrained("roberta-base")
    model = RobertaModel.from_pretrained("microsoft/graphcodebert-base")
    model = Model(model)
    model.load_state_dict(torch.load(args.model_path),strict=False) 
    model = model.to(args.device)
    model.eval()
    
    with torch.no_grad():

        # user side data 
        a_dataset = TextDataset(args, tokenizer, data_type = args.user_data_type, repeat = True, dataset = user_data, data_size = len(QAset), pool= pool)
        a_sampler = SequentialSampler(a_dataset)
        a_dataloader = DataLoader(a_dataset, sampler = a_sampler, batch_size = args.batch_size, num_workers=4)
        
        # forum side data 
        b_dataset = TextDataset(args, tokenizer, data_type = args.forum_data_type, repeat = False, dataset = QAset, data_size = len(QAset), pool = pool)
        b_sampler = SequentialSampler(b_dataset)
        b_dataloader = DataLoader(b_dataset, sampler = b_sampler, batch_size = args.batch_size, num_workers = 4)
        
        model.eval()
        a_vecs, b_vecs = [], [] 
        url_list = [] 

        # get the vector of input               
        for batch in a_dataloader:
            if args.user_data_type == "cod" or args.user_data_type == "all_data":
                a_inputs = batch[0].to(args.device)
                attn_mask = batch[1].to(args.device)
                position_idx = batch[2].to(args.device)
                a_vec = model(a_inputs, attn_mask, position_idx)
            else:
                a_inputs = batch[3].to(args.device)
                a_vec = model(nl_inputs = a_inputs)
            a_vecs.append(a_vec.cpu().numpy())
            
        for batch in b_dataloader:
            if args.forum_data_type == "cod":
                b_inputs = batch[0].to(args.device) 
                attn_mask = batch[1].to(args.device)
                position_idx = batch[2].to(args.device)
                b_vec = model(b_inputs, attn_mask, position_idx) 
            else:
                b_inputs = batch[3].to(args.device)
                b_vec = model(nl_inputs = b_inputs)
            b_vecs.append(b_vec.cpu().numpy())
            url = batch[4]
            url = list(url)
            url_list.extend(url) 
        
        a_vecs = np.concatenate(a_vecs, 0)
        b_vecs = np.concatenate(b_vecs, 0)
    
        if len(a_vecs) > args.tensor_limit:
            con_turn = int(len(a_vecs) / args.tensor_limit) 
            if len(a_vecs) % args.tensor_limit != 0:
                con_turn += 1 
            
            start = 0 
            t_limit = args.tensor_limit 
            for t in range(con_turn):
                if t == con_turn - 1:
                    tmp_a_vecs = a_vecs[start:]
                    tmp_b_vecs = b_vecs[start:]
                else:
                    tmp_a_vecs = a_vecs[start:t_limit]
                    tmp_b_vecs = b_vecs[start:t_limit]  
                    start = t_limit 
                    t_limit = start + args.tensor_limit  
            
                result_dict = matrix_multiplication(tmp_a_vecs, tmp_b_vecs, result_dict, t, url_list) 
        else:
            result_dict = matrix_multiplication(a_vecs, b_vecs, result_dict, 0, url_list)
            
    return result_dict
    
def graphcodebert_inference_main(args):
    
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = f"{args.device_id}"
    device = "cuda:0"
    args.device = device 
                 
    # data_type: description, code, log, console output 
    st_path = f"../data/{args.system}/{args.forum_data_type}.pickle"
    with open(st_path, "rb") as fr:
        QAset = pickle.load(fr)
        
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path, exist_ok=True)
    
    if args.user_data_type == "cod":
        f_user_data = [args.user_data]
    else:
        f_user_data = sent_tokenize(args.user_data)

    print(f_user_data)
        
    total_result_dict = {}
    for user_data in f_user_data:
        result_dict = {}
        result_dict = classify_code_and_questions(user_data, args, QAset, result_dict)
        
        for url, value in result_dict.items():
            if url in total_result_dict:
                total_result_dict[url] = total_result_dict[url] + value 
            else:
                total_result_dict[url] = value 
        
    total_result_dict = OrderedDict(sorted(total_result_dict.items(), key = lambda item:item[1], reverse = True))
    ranking_dict = scoring_func.get_ranked_dict(total_result_dict)
    
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path)
    
    with open(f'../{args.save_path}/{args.user_data_type}_{args.forum_data_type}.pickle', "wb") as fw:
        pickle.dump(ranking_dict, fw)
                        
graphcodebert_inference_main(args)
