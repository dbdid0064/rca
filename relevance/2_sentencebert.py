import pickle 
import argparse
import os, sys, pickle 
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from utils import scoring_func
from collections import OrderedDict
from sentence_transformers import SentenceTransformer, util
from itertools import repeat 
import numpy as np 
from nltk.tokenize import sent_tokenize

parser = argparse.ArgumentParser()
parser.add_argument("--system")
parser.add_argument("--user_data_type")
parser.add_argument("--user_data")
parser.add_argument("--forum_data_type")
parser.add_argument("--save_path")
parser.add_argument("--device_id")
parser.add_argument("--model_name_or_path")
args = parser.parse_args()

def scoring_result_dict(score, url_list):
    r_dict = {}
    for s_idx, s in enumerate(score):
        url = url_list[s_idx]
        if url in r_dict:
            if r_dict[url] < s:
                r_dict[url] = s.item()
        else:
            r_dict[url] = s.item()
    return r_dict


def sbert_compute_relevance(input, post_vec, url_list, args):
    
    # load model 
    model = SentenceTransformer(args.model_name_or_path)
    model.to(args.device)

    # encoding (embedding) input 
    input_vec = model.encode(input)
    
    # dot scoring between input and post vector 
    scores = util.dot_score(input_vec, post_vec)
    
    result_dict = {}

    for score in scores:
        r_dict = scoring_result_dict(score, url_list)
        for url, value in r_dict.items():
            
            if url in result_dict:
                result_dict[url] = result_dict[url] + value 
            else:
                result_dict[url] = value 
   
    result_dict = OrderedDict(sorted(result_dict.items(), key = lambda item:item[1], reverse = True))
            
    '''ranking_dict = scoring_func.get_ranked_dict(result_dict)
    
    with open(f'../{args.save_path}/{args.user_data_type}_{args.forum_data_type}.pickle', "wb") as fw:
        pickle.dump(ranking_dict, fw)'''
    return result_dict
    
def sbert_main(args):

    os.environ["CUDA_VISIBLE_DEVICES"] = f"{args.device_id}"
    device = "cuda:0"
    args.device = device 
    
    if not os.path.exists(args.save_path):
        os.makedirs(args.save_path, exist_ok=True)
    
    
    #preprocessed stackoverflow post (post can be post, log, cns)
    data_path = f"../data/{args.system}/{args.forum_data_type}.pickle"
    with open(data_path, "rb") as fr:
        post = pickle.load(fr) 
    
    url_list, post_list = [], []
    for p in post:
        url_list.append(p['url'])
        post_list.append(p['text'])
        
    model = SentenceTransformer(args.model_name_or_path)
    model.to(args.device)

    post_vec = model.encode(post_list)
    
    #sbert_compute_relevance(benchmark_cases[0], post_list, url_list, args)   
    total_result_dict= {}
    for u_data in args.user_data:
        
        result_dict = sbert_compute_relevance(post_vec, url_list, args)
        for url, value in result_dict.items():
            if url in total_result_dict:
                total_result_dict[url]= total_result_dict[url] + value 
            else:
                total_result_dict[url]= value 
    
    tmp = OrderedDict(sorted(total_result_dict.items(), key = lambda item:item[1], reverse = True))
    r_dict = scoring_func.get_ranked_dict(tmp)
    
    with open(f'../{args.save_path}/{args.user_data_type}_{args.forum_data_type}.pickle', "wb") as fw:
        pickle.dump(r_dict, fw)

sbert_main(args)
