# pair or each data artifacts 
def model_list():
       
    model_dict = [
        "cod_tnd", 
        "cod_log", 
        "cod_cns", 
        "tnd_cod", 
        "log_cod", 
        "cns_cod", 
        "log_log",
        "log_tnd", 
        "des:log", 
        "des:tnd", 
        "des:cns", 
        "cns:tnd", 
        "cns:cns", 
        "des:cmd", 
        "cmd:tnd", 
        "cmd:cmd"    
    ]
    return model_dict
