# load all results 
import pickle
import argparse
from itertools import repeat 
import os.path 
from os import path 
import multiprocessing
import sys 
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from s1_scoring import scoring_func 
import models_conf, weights_conf 
from scipy.stats import gmean 
from collections import OrderedDict

parser = argparse.ArgumentParser()
parser.add_argument("--system")
args = parser.parse_args()

def find_result(p):
    
    url = p['url']
    
    ranking_list, weights_list = [], []
    
    # get weight of each model 
    weight_conf = getattr(weights_conf, "model_weight")()
    # get all model 
    model_list = getattr(models_conf, "model_list")()
    for model_idx, model in enumerate(model_list):
        with open('./{args.result_path}/{model}.pickle', "rb") as fr:
            ranking_dict = pickle.load(fr)
        
        if url in ranking_dict:
            ranking_list.append(ranking_dict[url])
            weights_list.append(weight_conf[model_idx])
    
    combined_result = gmean(ranking_list, weights_list)
    
    return combined_result 
    
def load_result_main():
        
    # load all post of this application 
    post_path =f"../data/{args.system}/post.pickle"
    with open(post_path, "rb") as fr:
        post = pickle.load(fr)

    cpu_count = multiprocessing.cpu_count()
    pool = multiprocessing.Pool(processes=cpu_count)
    c_result = pool.starmap(find_result, post)
    pool.close()
    pool.join()
    
    r_dict = {}
    for idx, p in enumerate(post):
        url = p['url']
        r_dict[url] =  c_result[idx]

    s_r_dict = OrderedDict(sorted(r_dict.items(), key = lambda item:item[1], reverse = False))
    ranking_dict = scoring_func.get_ranked_dict(s_r_dict)
    
    for r, url in ranking_dict.items():
        print(f"{r}: {url}")    
    
load_result_main()
