
import pandas as pd
import pickle
import random
import re
import argparse
import os 

parser = argparse.ArgumentParser()
# same as user side 
parser.add_argument("--source_data")
# same as forum side 
parser.add_argument("--target_data")
args = parser.parse_args() 

from_data = args.source_data  
target = args.target_data 

targets = [target]
systems_ = ["openstack", "mongodb", "elasticsearch", "kafka", "spark"]
reg = r"[^a-zA-Z\s]"

# preprocess stackoverflow data 
def preprocess(txt):
    txt = txt.replace('[CODE]', '').replace('[/CODE]', '').replace('[QUOTE]', '').split()
    t_arr = []
    for t in txt:
        if len(t) == 0:
            continue
        new_str = re.sub(reg, " ", t).strip()
        #print(new_str)
        if len(new_str) != 0:
            t_arr.append(new_str)
    result_text = ' '.join(t_arr).lower()
    return ' '.join(result_text.split())

def is_code_unique(url):
    if url_count[url] == 1:
        return True
    return False



for target in targets:
    print(f'{from_data}-{target}')
    pos_data, neg_data = [], [] 
    for system_ in systems_:
        trainset_rate = 0.8

        with open(f'../data/{system_}/{from_data}.pickle', 'rb') as f:
            codes = pickle.load(f)
        with open(f'../data/{system_}/cmd.pickle', 'rb') as f:
            cmds = pickle.load(f)
        with open(f'../data/{system_}/log.pickle', 'rb') as f:
            logs = pickle.load(f)
        with open(f'../data/{system_}/cns.pickle', 'rb') as f:
            cns = pickle.load(f)

        with open(f'../data/{system_}/{target}.pickle', 'rb') as f:
            target_data = pickle.load(f)

        t_dict = {}
        for t in target_data:
            if t['url'] in t_dict:
                t_dict[t['url']].append(t['text'])
            else:
                t_dict[t['url']] = [t['text']]


        url_count = {}
        for c in codes:
            if c['url'] in url_count:
                url_count[c['url']] += 1
            else:
                url_count[c['url']] = 1
        

        #pos_data = []
        #neg_data = []


        data_size = len(target_data)
        tmp = 0
        for c in codes:
            if not is_code_unique(c['url']):
                continue
            data_len = 0
            #print(c['url'], c['text'])
            if c['url'] in t_dict:
                co = preprocess(c['text'])
                if len(co) < 10: continue
                for data in t_dict[c['url']]:
                    nlq = preprocess(data)
                    if len(nlq) >= 10:
                        data_len += 1
                        pos_data.append([c['url'], co, nlq, 1])
            tmp += 1
            for i in range(data_len):
                while True:
                    rand = random.randint(0, data_size-1)
                    while True:
                        if target_data[rand]['url'] != c['url']:
                            break
                        rand = random.randint(0, data_size-1)
                    nlq = preprocess(target_data[rand]['text'])
                    #print(target_data[rand]['text'])
                    if len(nlq) >= 10:
                        neg_data.append([c['url'], co, nlq, 0])
                        break


    pos_df = pd.DataFrame(pos_data, columns=['url', 'code', 'nl_query', 'label'])
    neg_df = pd.DataFrame(neg_data, columns=['url', 'code', 'nl_query', 'label'])


    pos_train_size = int(len(pos_df) * 0.8)
    neg_train_size = int(len(neg_df) * 0.8)
    pos_test_data = pos_df[pos_train_size:]
    pos_train_data = pos_df[:pos_train_size]

    neg_test_data = neg_df[neg_train_size:]
    neg_train_data = neg_df[:neg_train_size]


    train_data = pd.concat([pos_train_data, neg_train_data])
    test_data = pd.concat([pos_test_data, neg_test_data])

    train_data = train_data.sample(frac=1).reset_index(drop=True)
    test_data = test_data.sample(frac=1).reset_index(drop=True)

    if not os.path.exists("../train_data/"):
        os.makedirs("../train_data/")

    with open(f'../train_data/{from_data}_{target}_train.pickle', 'wb') as f:
        pickle.dump(train_data, f)

    with open(f'../train_data/{from_data}_{target}_test.pickle', 'wb') as f:
        pickle.dump(test_data, f)

    #print(f'{target} - {system_}')
    #print(len(train_data))
    #print(len(test_data))
    print(f'{system_}: {len(pos_df)}')
