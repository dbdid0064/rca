
def remove_hyperlink(text):
    text = text.split()
    word_list = []
    for word in text:
        if "http:" in word or "https:" in word:
            continue  
        word_list.append(word)
    
    text = ' '.join(word_list) 
    return text 
        
def remain_alpha(text):

    pos_punc = ["@", ".", ",", ":", "!", "?", ";", '"', "{", "}", "[", "]", "(", ")", "<", ">", "_", "-", "'", "’", "‘", "/"]
    flag_punc = [".", ",", ":", "!", "?", ";", "_", "*"]

    word_in_text = text.split()
    word_list = []
    for word in word_in_text:
        n_word = word 
        flag = False 
        for punc in pos_punc:
            if punc in word:
                if punc in flag_punc:
                    flag = True 
                n_word = n_word.replace(punc, " ")
        n_word = remove_blank(n_word)
        not_word = []
        for idx, w in enumerate(n_word):
            if not w.isalpha():
                not_word.append(idx)
            
        n_word = word 
        if flag == True:
            for punc in pos_punc:
                if punc in flag_punc:
                    continue 
                else:
                    n_word = n_word.replace(punc, " ")
        else:
            for punc in pos_punc:
                n_word = n_word.replace(punc, " ")
        n_word = remove_blank(n_word)
        
        for s_idx, w in enumerate(n_word):
            if s_idx in not_word:
                continue  
            if w == "u":
                continue  
            word_list.append(w)
        
    text = ' '.join(word_list)
    return text 

# replace number 
def replace_number(text):
    
    number_list = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
    word = text.split(' ')
    s_list =[]
    for w in word:
        flag = True 
        # remove alphabet with numbert 
        for number in number_list:
            if number in w:
                flag = False 
        if flag == True:
            s_list.append(w)                

    new_text = ' '.join(s_list)
    return new_text

# remove blank in text 
def remove_blank(text):
    
    word = text.split(" ")
    n_word = []
    for w in word:
        if len(w) == 0:
            continue  
        else:
            n_word.append(w)
    return n_word

# tokenize with blank and puncuations
def tokenize_with_punc(text):
    
    text_list = [text]
    sent_list, t_sent_list = [], []
    for text in text_list:
        text = text.split(" ")
        for word in text:
            if len(word) == 0:
                continue
            if "." in word and ":" in word:
                t_sent_list.append(word) 
                sentence = ' '.join(t_sent_list)
                if len(sentence) == 0:
                    continue  
                sent_list.append(sentence)
                t_sent_list = []
            if word[len(word) - 1] == ":":
                t_sent_list.append(word)
                sentence = ' '.join(t_sent_list)
                if len(sentence) == 0:
                    continue  
                sent_list.append(sentence)
                t_sent_list = []
                continue  
            if '.' in word and word[len(word) - 1] == '.':
                t_sent_list.append(word) 
                sentence = ' '.join(t_sent_list)
                if len(sentence) == 0:
                    continue  
                sent_list.append(sentence)
                t_sent_list = []
                continue 
            t_sent_list.append(word)
    sentence = ' '.join(t_sent_list)
    if len(sentence) != 0:
        sent_list.append(sentence)
    return sent_list                 

# remove only variables 
def remove_variables(text):
    text = text.lower()
    text = remove_hyperlink(text)
    text = remain_alpha(text)
    text = replace_number(text)
    text = remove_blank(text)
    text = ' '.join(text)
    
    return text

# remove variables and split with punc 
def remove_variables_and_split_punc(text):
    
    text = remove_variables(text)
    # return of tokenize_with_pucn is 'list' 
    text = tokenize_with_punc(text)

    return text 
