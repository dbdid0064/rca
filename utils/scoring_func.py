def get_ranked_dict(result_dict):
    
    ranking_dict = {}
    ranking = 0
    for url, value in result_dict.items():
        if ranking == 0:
            last_value = value 
            ranking = ranking + 1
            n = 1
        else:
            if last_value != value:
                ranking = n + 1 
                n = ranking 
                last_value = value 
            else:
                n = n + 1 
        ranking_dict[url] = ranking 
        
    return ranking_dict 

# get mrr of ranked result 
def mrr_v1(rankings):
    
    tot = 0 
    for rank in rankings:
        if rank <= 0:
            continue 
        tot = tot + (1 / rank) 
        
    mrr = tot / len(rankings)
    return mrr 

# get succrate@k of ranked result
def succrate_k(rankings, k):
    
    cnt = 0 
    for rank in rankings:
        if rank <= 0:
            continue    
        if rank <= k:
            cnt = cnt + 1
            
    succ = cnt / len(rankings)
    return succ, cnt 

