#!/bin/bash

# training graphcodebert 
model_type="graphcodebert"
config_name="microsoft/graphcodebert-base"
model_name_or_path="microsoft/graphcodebert-base"
tokenizer_name="microsoft/graphcodebert-base"
lang="python"
num_train_epochs=10
code_length=256
data_flow_length=64
nl_length=128
train_batch_size=32
eval_batch_size=32
learning_rate=2e-5
seed=123456
device_id="2"

output_dir="./models/${source_type}_${target_type}"
train_data_file="../train_data/${source_type}_${target_type}_train.pickle"
test_data_file="../train_data/${source_type}_${target_type}_test.pickle"

python3 graph_fine_tuning.py --output_dir ${output_dir} --config_name ${config_name} --model_name_or_path ${model_name_or_path} --tokenizer_name ${tokenizer_name} --lang ${lang} --do_train --train_data_file ${train_data_file} --eval_data_file ${test_data_file} --num_train_epochs ${num_train_epochs} --code_length ${code_length} --data_flow_length ${data_flow_length} --nl_length ${nl_length} --train_batch_size ${train_batch_size} --eval_batch_size ${eval_batch_size} --learning_rate ${learning_rate} --device_id ${device_id} --seed ${seed} 
