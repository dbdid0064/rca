#!/bin/bash
model_type="roberta"
task_name="codesearch"
data_dir="../train_data/"
max_seq_length=512
per_gpu_eval_batch_size=32
per_gpu_train_batch_size=32
num_train_epochs=8
gradient_accumulation_steps=1
train_batch_size=32
eval_batch_size=32
learning_rate=2e-5
seed=123456
device_id="1"
nl_query_length=100 
model_name_or_path="microsoft/codebert-base"
c_model_type="codebert"


train_file="${source_type}_${target_type}_train.pickle"
test_file="${source_type}_${target_type}_test.pickle"
output_dir="./models/${source_type}_${target_type}"

python3 codebert_fine_tuning.py --model_type ${model_type} --task_name ${task_name} --do_train --do_eval --eval_all_checkpoints --train_file ${train_file} --dev_file ${test_file} --max_seq_length ${max_seq_length} --per_gpu_train_batch_size ${per_gpu_train_batch_size} --per_gpu_eval_batch_size ${per_gpu_eval_batch_size} --learning_rate ${learning_rate} --num_train_epochs ${num_train_epochs} --gradient_accumulation_steps ${gradient_accumulation_steps} --overwrite_output_dir --data_dir ${data_dir} --output_dir ${output_dir} --model_name_or_path ${model_name_or_path} --device_id ${device_id} --nl_query_length ${nl_query_length} 
