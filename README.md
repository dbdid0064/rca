# FDD

FDD:Fine-granular Data Retrieval of Online Forum Knowledge with Language Models for System Troubleshooting

### Dependency

```
pip install -r requirements.txt
```

### Code & Directory Structures 

- ```./data```: data of code(cod), command(cmd), console output(cns), log(log) and title and description(tnd) in stackoverflow tagged with 'openstack' 
because the data is too large, we only upload the data of 'openstack' 
- ```./models```: fine-tuned model will be saved 
- ```./utils```: preprocessing, scoring function, make train data and temp files 
- ```./relevance```: compute relevance between user-side data and forum-side data with multiple models (CodeBERT, GraphCodebert, BM25, SentenceBERT)
- ```./aggregate_result```: aggregate result of all models 
- ```./train```: train (fine-tuning) bert-based models

### Usage 

#### Step1. make fine-tuning dataset & fine-tuning models
In this step, we make fine-tuning dataset to train model. 
```python
./utils/
python3 make_train_dataset.py --source_data cod --target_data tnd 
```
After creating fine-tuning dataset, we train model using fine-tuning datasets. 
```
./train/
source_data=cod 
target_data=tnd
bash codebert_train.sh 
bash graphcodebert_train.sh
```
source_data and target data can be any types of data. 

#### Step2. Compute relevance between user-side data and forum-side data 
After fine-tuning models, we compute relevance between user-side data and forum-side data.
- compute relevance between ```cod:tnd```, ```des:cod``` using fine-tuned CodeBERT. 
```python
./relevance/
python3 1_codebert_inf.py 
    --system openstack 
    --user_data_type cod 
    --user_data "import openstack os_connect = openstack.connect(auth_url = AUTH_URL, project_name = PROJECT_NAME, username = USERNAME, password = PASSWORD, region_name = REGION_NAME, user_domain_name = USER_DOMAIN_NAME, app_version = 1.0)"
    --forum_data_type tnd 
    --save_path ./result/
    --device_id 1
    --model_name_or_path cod_tnd 
    --batch_size 32 
    --tokenizer_name roberta-base 
```
- compute relevance between ```cod:log```, ```log:cod```, ```cns:cod```, ```cod:cns``` using fine-tuned GraphCodeBERT. 
```python
./relevance/
python3 1_graphcodebert_inf.py 
    --system openstack 
    --model_path ./model/
    --user_data_type cns
    --user_data "keystoneauth1.exceptions.http.Unauthorized: The request you have made requires authentication. (HTTP 401) (Request-ID)"
    --forum_data_type cod
    --user_data_length 128
    --forum_data_length 256 
    --lang python 
    --save_path ./result/
    --device_id 1
    --model_type cod_cns  
    --batch_size 32 
```
- compute relevance between ```log:log```, ```log:tnd```, ```des:log```, ```cns:cns```, ```cns:tnd```, ```des:cns```, ```des:tnd``` using SentenceBERT.
```python
./relevance/
python3 2_sentencebert.py 
    --system openstack 
    --user_data_type cns
    --user_data "keystoneauth1.exceptions.http.Unauthorized: The request you have made requires authentication. (HTTP 401) (Request-ID)"
    --forum_data_type cns
    --save_path ./result/
    --device_id 1
    --model_name_or_path all-MiniLM-L6-v2 
```
- compute relevance between ```cmd:tnd```, ```cmd:cmd```, ```des:cmd``` using BM25. 
```python
./relevance/
python3 3_bm25.py 
    --system openstack 
    --user_data_type cmd
    --user_data "openstack server create --image --flavor --security-group --block-device-mapping --nic --network --port --config-drive <server-name>"
    --forum_data_type cmd
    --save_path ./result/
```
#### Step3. Aggregate results 

After computing all relevance between data types, we aggregate results and get ranked list of forum data.

```python
./aggregate_result/
python3 load_result.py 
    --system openstack 
```

This step shows the ranked list of relevant questions. 
```
********************Results********************
1: https://stackoverflow.com/questions/70505704
2: https://stackoverflow.com/questions/41480251
3: https://stackoverflow.com/questions/45309827
4: https://stackoverflow.com/questions/46907661
5: https://stackoverflow.com/questions/44701910
6: https://stackoverflow.com/questions/71732392
7: https://stackoverflow.com/questions/54224530
8: https://stackoverflow.com/questions/55756904
9: https://stackoverflow.com/questions/63749573
10: https://stackoverflow.com/questions/46721423
```
The relevant question of this benchmark case is ranked at 4th (<https://stackoverflow.com/questions/46907661>). 

#### data 
data of code(cod), command(cmd), console output(cns), log(log) and title and description(tnd) in stackoverflow tagged with 'openstack'.
- ```cod.pickle```: code in questions 
- ```log.pickle```: log in questions 
- ```cmd.pickle```: command in questions 
- ```cns.pickle```: console output in questions 
- ```tnd.pickle```: title and description in questions 
